﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BasicAgent : MonoBehaviour {

	public Transform desitnation;
	private NavMeshAgent nevMeshAgent;

	// Use this for initialization
	void Start () {

		nevMeshAgent = GetComponent<NavMeshAgent> ();
	}
	
	// Update is called once per frame
	void Update () {

		nevMeshAgent.SetDestination (desitnation.position);
	}
}
