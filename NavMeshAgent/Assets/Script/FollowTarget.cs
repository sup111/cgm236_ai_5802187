﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour {
	public float speed;
	public Transform destination;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.LookAt (destination);
		if (Vector3.Distance (destination.position, transform.position) > 0.1) {
			transform.Translate (Vector3.forward * Time.deltaTime * speed);
		}
	}
}
