﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softing_5802187
{
    class Class_Array
    {
        int[] Array;
        int Upper;
        int NumElements;

        public Class_Array(int size)
        {
            Array = new int[size]; // Array size , beginning at 0
            Upper = size - 1; // top address 
            NumElements = 0; //current address
        }
        //insert
        public void Insert (int item)
        {
            Array[NumElements] = item;
            NumElements++;
        }
        //display
        public void ReturnElements()
        {
            for (int i = 0; i<=Upper ;i++) //begin i=0, stop i<Upper
            {
                Console.Write(Array[i] +" "); // display all array value
            }
        }
        //clear
        public void Clear()
        {
            for (int i = 0; i <= Upper; i++)
            {
                Array[i] = 0; // change value to 0
            }
            NumElements = 0; //reset NumElements
        }

        public void BubbleSort()
        {
            int temp, outer, inner;

            for(outer=Upper; outer>=-1; outer--) // upper = size -1
            {
                for(inner = 0; inner<= outer-1; inner++) 
                {
                    if((int)Array[inner] > Array[inner+1])
                    {
                        temp = Array[inner];
                        Array[inner] = Array[inner + 1];
                        Array[inner + 1] = temp;
                    }
                }

                this.ReturnElements(); // Class_Array -> ReturnElements()
            }
        }

        public void SelectionSort()
        {
            int min, outer, inner, temp;

            for(outer = 0; outer<=Upper;outer++) //upper = size -1
            {
                min = outer;
                for(inner = outer+1 ;inner<=Upper;inner++)
                {
                    if (Array[inner] < Array[min]) // analysis
                        min = inner; //
                    temp = Array[outer];
                    Array[outer] = Array[min];
                    Array[min] = temp;
                }
            }
        }

        public void InsertionSort()
        {
            int inner, outer, temp;

            for(outer = 1; outer<= Upper; outer++)
            {
                temp = Array[outer];
                inner = outer;

                while(inner>0 && Array[inner - 1] >= temp)
                {
                    Array[inner] = Array[inner - 1];
                    inner -= 1; // reduce inner value by 1
                }
                Array[inner] = temp;
            }
        }
    }
}
