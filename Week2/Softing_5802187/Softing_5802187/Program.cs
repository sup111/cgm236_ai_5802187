﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softing_5802187
{
    class Program
    {
        static void Main(string[] args)
        {
            Class_Array nums = new Class_Array(10); //array = 10
            Random RandomValue = new Random(100); //Random value 1-100;

            for (int i = 0; i < 10; i++)
                nums.Insert((int)(RandomValue.NextDouble() * 100));
            
            Console.WriteLine("Before sorting : ");
            nums.ReturnElements(); //Class_Array -> void ReturnElement()            
                                   //Display array
                                   //BUBBLESORT
                                   // Console.WriteLine("");
                                   //    Console.WriteLine("During Softing : ");
                                   //    nums.BubbleSort(); //Class_Array -> void BubbleSort()

            //nums.SelectionSort(); //Class_Array -> void SelectionSort();

            nums.InsertionSort();

            Console.WriteLine("");
            Console.WriteLine("After Sorting : ");
            nums.ReturnElements(); //Class_Array -> void ReturnElement()     
            
            Console.Read();
        }
    }
}
