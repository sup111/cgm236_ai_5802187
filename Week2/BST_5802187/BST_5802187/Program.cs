﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BST_5802187
{
    class Program
    {
        static void Main(string[] args)
        {
            BinarySearchTree nums = new BinarySearchTree();
            nums.Insert(23);
            nums.Insert(40);
            nums.Insert(20);
            nums.Insert(30);
            nums.Insert(5);
            nums.Insert(80);
            nums.Insert(21);

            //Console.WriteLine("BST : Inorder");
            //nums.InOrder(nums.root);

            //Console.WriteLine("BST : Preorder");
            //nums.PreOrder(nums.root);

            //Console.WriteLine("BST : Postorder");
            //nums.PostOrder(nums.root);

            //Console.WriteLine("MIN IS :");
            //Console.WriteLine(nums.FindMin());

            Console.WriteLine("MAX IS :");
            Console.WriteLine(nums.FindMax());

            Console.Read();
        }
    }
}
