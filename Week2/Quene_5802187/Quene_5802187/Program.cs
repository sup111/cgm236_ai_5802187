﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quene_5802187
{
    class Program
    {
        static void Main(string[] args)
        {
            CQuene myQuene = new CQuene();
            string choice, value;

            string word = "Yandere";

            for (int i= 0; i < word.Length; i++)
            {
                myQuene.Enquene(word.Substring(i, 1));
            }

            while(true)
            {
                Console.WriteLine("~~~~~");
                Console.WriteLine("(f) front");
                Console.WriteLine("(e) enquene");
                Console.WriteLine("(d) dequene");
                Console.WriteLine("(c) count");

                choice = Console.ReadLine();
                choice = choice.ToLower();

                char[] onecher = choice.ToCharArray();
                
                switch(onecher[0])
                {
                    case 'f':
                        Console.WriteLine("front : " + myQuene.Front());
                        break;

                    case 'e':
                        Console.WriteLine();
                        Console.WriteLine("Enter value to Enquene : ");
                        value = Console.ReadLine();

                        myQuene.Enquene(value);

                        Console.WriteLine("Enquene: " + value);
                        break;

                    case 'd':
                        myQuene.Dequene();
                        Console.WriteLine("Dequene");
                        break;

                    case 'c':
                        Console.WriteLine("Count: " + myQuene.Count);
                        break;
                }                
            }
        }
    }
}
