﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Quene_5802187
{
    class CQuene
    {
        ArrayList pquene;

        public CQuene()
        {
            pquene = new ArrayList();
        }
        public void Enquene(Object item)
        {
            pquene.Add(item);
        }
        public void Dequene()
        {
            pquene.RemoveAt(0);
        }
        public Object Front()
        {
            return pquene[0];
        }
        public int Count
        {
            get { return pquene.Count; }
        }
        public void ClearQuene()
        {
            pquene.Clear();
        }
    }
}
