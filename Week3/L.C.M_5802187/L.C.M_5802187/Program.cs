﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L.C.M_5802187
{
    class Program
    {
        static void Main(string[] args)
        {
            int num1,num2,a,b;
            int lcm = 0;

            Console.WriteLine("Enter the First Number : ");
            num1 = int.Parse(Console.ReadLine());
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

            Console.WriteLine("Enter the Second Number : ");
            num2 = int.Parse(Console.ReadLine());
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

            a = num1;
            b = num2;
            while(num1 != num2)
            {
                if(num1 > num2)
                {
                    num1 = num1 - num2; 
                }
            }

            lcm = (a * b) / num1;

            Console.WriteLine("Lowest Common Mutiple is : "+ lcm);
            Console.ReadLine();

        }
    }
}
