﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H.C.F_5802187
{
    class Program
    {
        
        public static void Main(string[] args)
        {
            int num1, num2, num3;
            int hcf = 0;

            Console.WriteLine("Enter the First Number : ");
            num1 = int.Parse(Console.ReadLine()); //num1
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

            Console.WriteLine("Enter the Second Number : ");
            num2 = int.Parse(Console.ReadLine()); //num2
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

            Console.WriteLine("Enter the Third Number : ");
            num3 = int.Parse(Console.ReadLine()); //num3
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

            for (int i = 1; i <= num1 || i <= num2 || i <= num3 ; ++i)
            {
                if (num1%i==0 && num2% i ==0 && num3 % i == 0)
                hcf = i; //update value
            }

            Console.WriteLine("Highest Common Factor is : " + hcf);
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            Console.ReadLine();
        }
    }
}
