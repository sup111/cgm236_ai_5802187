﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueHit : BlueGhost {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter2D(Collider2D target)
    {
        if (target.gameObject.tag == "TP")
        {
            CanTurn = true;
        }
    }
    void OnTriggerExit2D(Collider2D target)
    {
        if (target.gameObject.tag == "TP")
        {
            CanTurn = false;
        }
    }
}
