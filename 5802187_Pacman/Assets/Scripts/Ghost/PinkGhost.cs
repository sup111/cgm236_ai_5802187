﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinkGhost : MonoBehaviour {

    public float speed;

    private Animator anim;

    private bool up = false;
    private bool down = false;
    private bool left = false;
    private bool right = true;

    private bool CanMove = true;
    public static bool CanTurn;

    private int Dir;
    public float Delay;

    private int OldDir;
    void Start()
    {
        Dir = 1;
        OldDir = Dir;
        Delay = Time.time;

        anim = GetComponent<Animator>();
        Physics2D.IgnoreLayerCollision(9, 9, true);
    }


    void Update()
    {
        if (Dir < 0)
        {
            Dir = 0;
        }
        else if (Dir > 3)
        {
            Dir = 3;
        }

        Move();
        ChangeDirection();
        LoopWarp();
    }

    void Move()
    {

        if (CanMove == true)
        {
            if (right == true)
            {
                transform.position += Vector3.right * speed * 0.1f;
            }
            else if (left == true)
            {
                transform.position += Vector3.left * speed * 0.1f;
            }
            else if (up == true)
            {
                transform.position += Vector3.up * speed * 0.1f;
            }
            else if (down == true)
            {
                transform.position += Vector3.down * speed * 0.1f;
            }
            else
            {

            }

        }
    }

    void ChangeDirection()
    {
        float DelayTime = Random.Range(0.5f, 1);
        if (CanTurn == true && Time.time >= Delay)
        {
            Dir = Random.Range(0, 4);
            if (Dir == OldDir)
            {
                return;
            }
            Delay = Time.time + DelayTime;

            OldDir = Dir;
            if (Dir == 0)
            {
                up = true;
                down = false;
                left = false;
                right = false;
                anim.SetFloat("DirX", 0.0f);
                anim.SetFloat("DirY", 2.0f);
            }
            else if (Dir == 1)
            {
                up = false;
                down = false;
                left = false;
                right = true;
                anim.SetFloat("DirX", 2.0f);
                anim.SetFloat("DirY", 0.0f);
            }
            else if (Dir == 2)
            {
                up = false;
                down = true;
                left = false;
                right = false;
                anim.SetFloat("DirX", 0.0f);
                anim.SetFloat("DirY", -2.0f);
            }
            else if (Dir == 3)
            {
                up = false;
                down = false;
                left = true;
                right = false;
                anim.SetFloat("DirX", -2.0f);
                anim.SetFloat("DirY", 0.0f);
            }
        }
    }

    void LoopWarp()
    {
        if (transform.position.x < -5.5)
        {
            transform.position = new Vector2(6f, transform.position.y);
        }
        if (transform.position.x > 6.1)
        {
            transform.position = new Vector2(-5.4f, transform.position.y);
        }
    }

    void OnCollisionEnter2D(Collision2D target)
    {
        if (target.gameObject.tag == "Wall")
        {
            CanTurn = true;
        }
    }
    void OnCollisionStay2D(Collision2D target)
    {
        if (target.gameObject.tag == "Wall")
        {
            CanTurn = true;
        }
    }
}
