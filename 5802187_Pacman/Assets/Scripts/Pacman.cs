﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pacman : MonoBehaviour {

    public float speed;

    private Animator anim;

    private bool up = false;
    private bool down = false;
    private bool left = false;
    private bool right = true;

    private bool CanMove = true;
    public static bool CanTurn;

    void Start () {
        anim = GetComponent<Animator>();
		Physics2D.IgnoreLayerCollision (8, 9, false);
    }


    void Update () {

        MovePacman();
        ChangeDirection();
        LoopWarp();        
     }

    void MovePacman() {

        if (CanMove == true) {
            if (right == true)
            {
                transform.position += Vector3.right * speed * 0.1f;
            }
            else
            {

            }

            if (left == true)
            {
                transform.position += Vector3.left * speed * 0.1f;
            }
            else
            {

            }

            if (up == true)
            {
                transform.position += Vector3.up * speed * 0.1f;
            }
            else
            {

            }

            if (down == true)
            {
                transform.position += Vector3.down * speed * 0.1f;
            }
            else
            {

            }

        }
    }

    void ChangeDirection()
    {
        if (CanTurn == true)
        {
            if (Input.GetKey(KeyCode.UpArrow))
            {
                up = true;
                down = false;
                left = false;
                right = false;
                anim.SetFloat("DirX", 0.0f);
                anim.SetFloat("DirY", 2.0f);
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                up = false;
                down = false;
                left = false;
                right = true;
                anim.SetFloat("DirX", 2.0f);
                anim.SetFloat("DirY", 0.0f);
            }
            if (Input.GetKey(KeyCode.DownArrow))
            {
                up = false;
                down = true;
                left = false;
                right = false;
                anim.SetFloat("DirX", 0.0f);
                anim.SetFloat("DirY", -2.0f);
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                up = false;
                down = false;
                left = true;
                right = false;
                anim.SetFloat("DirX", -2.0f);
                anim.SetFloat("DirY", 0.0f);
            }
        }
    }

    void LoopWarp()
    {
        if(transform.position.x < -5.5)
        {
            transform.position = new Vector2(6f, transform.position.y);
        }
        if(transform.position.x > 6.1)
        {
            transform.position = new Vector2(-5.4f, transform.position.y);
        }
    }


    
    void OnCollisionEnter2D(Collision2D target)
    {

        if (target.gameObject.tag == "Ghost")
        {
            CanMove = false;
			Physics2D.IgnoreLayerCollision (8, 9, true);
            anim.SetTrigger("IsDead");
            StartCoroutine(Reborn());
        }
    }


    IEnumerator Reborn()
    {
        yield return new WaitForSeconds(3f);
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
        CanMove = true;
        //anim.SetBool("IsDead", false);
        //transform.position = new Vector2(-0.31f, -0.66f);
    }
}


