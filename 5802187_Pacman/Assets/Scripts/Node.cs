﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{

    public Node[] PointWay;
    public Vector2[] Direction;

    // Use this for initialization
    void Start()
    {
        Direction = new Vector2[PointWay.Length];

        for(int i = 0; i< PointWay.Length; i++)
        {
            Node Way = PointWay[i];
            Vector2 tempVector = Way.transform.localPosition - transform.localPosition;

            Direction[i] = tempVector.normalized;
        }
    }

}
