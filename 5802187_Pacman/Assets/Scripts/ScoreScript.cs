﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour {

    public static int score;
    private int old;
    public Text Input_score; 


	// Use this for initialization
	void Start () {
        score = 0;
        old = score;
    }
	
	// Update is called once per frame
	void Update () {
        UpdateScore();
	}

    void UpdateScore()
    {
        if (old != score)
        {
            old = score;
            Input_score.text = "Score : " + score.ToString();
        }
    }
}
